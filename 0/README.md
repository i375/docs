# _input_output_hardware, _processor, _memory, & _code 

Everything can be ran-emulated-simulated in code/software except actual physical input and output which interacts with outer world. 

All you need is CPU, Memory and physical_IO_devices, and a programming_language to make software make valuable work for you.  
In other words all you need is some hardware which can run software and other hardware which could read stimulus from outer world and effector hardware which could send stimulus to outer world.  
Everything else like users, passwords, operating systems, API-s and so on is logistics and management. Useful logistics and management but not fundamentally required.  

*Just a fun fact.*
