# Recipe for doing STDIO with subprocesses (using pipes) in Python/PHP

_Example is based on Windows cmd.exe, but it translates to linux/macOS with little change_

The operation example that is in Python is equivalent of calling ```dir```
in current diretory and redirecting it's output to ```findstr``` to find lines
containing ```DIR```.
```
dir | findstr DIR
```

And the python code
```Python
from subprocess import *

#Here we execute       {dir} command 
#                        |     and setup PIPE for STDOUT
#                        |           |
p1 = Popen(["cmd","/C","dir"], stdout=PIPE)

#                                         Here we redirect output of dir to findstr
#                                           |        |
p2 = Popen(["cmd","/C","findstr","DIR"], stdin=p1.stdout, stdout=PIPE)
p1.stdout.close() # p1 will be notified about p2 exiting with SIGPIPE

#Here we "communicate" with 
#                  empty input with p2 (finstr) and get it's output
#                         |
p2Result = p2.communicate()[0]

#Finally printing results
print(p2Result)
```

And the PHP code:
```PHP
<?php

error_reporting(E_ALL);

//Handle will be ---------------------------------------->  readable
// |                                                             |
$pipeHandle = popen("cmd.exe /C dir | cmd.exe /C findstr DIR ", "r");

//Here we're able to read command 
//result
// |
$commandResult = fread($pipeHandle, 2048);

echo $commandResult;

pclose($pipeHandle);
```

That's just the basic recipe for getting started.

**Further reference material:**

[Lib/subprocess.py](https://docs.python.org/3.6/library/subprocess.html) 

[Pipe to a Subprocess](https://www.gnu.org/software/libc/manual/html_node/Pipe-to-a-Subprocess.html)
