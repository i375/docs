# VIM pocket cookbook

## Cursor movement 

In Vim arrow keys, cursor movement map to standard symbols:

```h``` move left; ```l``` move right; 

```j``` move down; ```k``` move up;

Arrow keys also work but they may mess up on some console amulators and systems.

## Undo, redo

For undoing type ```<ESC>, u``` and for redoing ```<ESC>, <CTRL>+r```.

## Copying, cutting and pasting text

To copy text type ```<ESC>, v```, to enter _-- VISUAL --_ mode and select text by moving cursor around (pressing h,l,j,or k) for copying/cutting. 

When done, press ```d``` for cutting or ```y``` for copying(yanking). 

Navigate where you want to paste the text and after press ```p``` for pasting after
cursor or ```P``` for pasting before cursor.

For deleting from cursor to the end of line type ```<ESC>, D```.

For deleting whole line type ```<ESC>, d, d```.

## Creating (spliting) and navigating vertical and horizontal sub windows

For spliting horizontally type ```<ESC>, <CTRL>+w``` and ```s```, or ```v``` for vertical split. 

To switch to desired sub-window type ```<ESC>, <CTRL>+w``` and ```<ARROWS>```. Or to just cycle 
through sub-sindows ```<ESC>, <CTRL>+w, <CTRL>+w, ...```.

To resize active window horizontally type ```<ESC>, :, vertical resize {number of collumns}``` or to resize all windows equally ```<ESC>, <CTRL>+w, =```.

## Creating and navigating tabs



## Edit current user's .vimrc (the vim startup script)

```<ESC>, :, edit $MYVIMRC``` 

## Find, and jump next or previous occurrence of string, and find and replace

```<ESC>, /{text to find}, <ENTER>``` then ```n``` to jump to next occurrence, or ```N``` to goto previous occurrence.

To find and replace occurances type ```<ESC>, :, %s/{text to find}/{text to replace with}/g```.

## Formatting text

To reindent text type ```<ESC>, :, 0, <ENTER>, vG``` select whole document and then ```=``` to reindent text. 
