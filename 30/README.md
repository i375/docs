# Computing field offset in C struct

**Aim:** _Compute field offset in C struct with ```struct``` and ```field``` names_

Idea is to cast any momory address in this case ```0``` to specific ```struct```
and then get any field address in this case ```str_1```. Then compute difference between
addresses of the field ```str_1``` and ```0```.

**Implementation:**
```CPP
#include <stdio.h>

typedef struct struct_1
{
  int a;
  char* str_1;  
}struct_1;

#define OFFSET_IN_STRUCT(struct_type, field_name) ((int)(&((struct_type*)(0))->field_name) - (int)0)

int main()
{

  printf("Offset of str_1 in struct_1 is: %d \n", OFFSET_IN_STRUCT(struct_1, str_i));

  return 0;
}
```
