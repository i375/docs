# Money, for ideas is what studio is for musicians

_Speculating on matter of money in entrepreneurial context_

Money is a resource a room full of instruments which you have to have to
"transform" ideas into matter interactable objects (writing on the paper,
audio file containing music, video file, game executable,...). And put
it in front of eyes, ears, skin, ... 

## There are no guarantees

Just like good studio is not a guarantee that good music(commercially
successful, quality, or both) will be produced with it, money too
is not a guarantee but a necessary room for doing the job and experiment.

## Vision and mind is the tool human "without money/studio" invests

Just like in case of studio (so for in case of money), vision, mind, and skill 
still is important makes difference. Weather is production of mass market
"bullshit product" or a master piece. It's the mind which will use tools
and arrange information and matter in a way that will stand out on market (in
minds of other people) or blend in on a massive scale to drive hype and profit.

## Mind is the resource to be treated like one treats money and even more preciously 

When one has to spend most of money on daily needs its hard to accumulate enough money for buying the studio, so is the mind, which too is a limited resource which if mostly is spent n doing daily routine (requiring most of attention) there won't be left enough of it to produce ideas that could be valuable and usable in case of having money at hand.

So mind and attentions needs to be nurtured and cared, at least after some point in time, just 
like money is nurtured when saving it for future plans and/or current needs in waiting queue.