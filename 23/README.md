# C++ templates with value and type parameters, and operator overloading sample.

```c++
#include <iostream>

class OutOfArrayBoundsAccess;

template
<
    typename ElementType, // This template parameter for setting array elements' type.
    unsigned int arraySize, // This template parameter for setting array elements count.
    bool boundsCheck // This parameter if for enabling or disablig bounds checkgin
> 
class TypedArraySafe { 
    const unsigned int elementCount = arraySize; // Using value type template parameter.
    ElementType arrayElements[arraySize]; // Using value type template parameter.

public:

    // Making possible accessing elements with <myArray[index]> form.
    //
    // Returning reference to item, so that write is possible.
    //                ↓
    inline ElementType& operator[](const unsigned int index) {

        // Here we decide weather we want bounds checking or not.
        //   | As for performance matter, when compiling with optimisation flags,
        //   | this <if> condition will be completelly gone from generated machine code,
        //   ↓ it's as fast as macro and event better in conjuction with <operator[]>.
        if (boundsCheck) {
            checkIfIndexInsideArrayBounds(index);
        }

        return this->arrayElements[index];
    }

    inline void checkIfIndexInsideArrayBounds(unsigned int index)
    {
        if (index > (this->elementCount - 1))
        {
            throw OutOfArrayBoundsAccess();
        }
    }

    inline int getElementCount() {
        return this->elementCount;
    }

};

class OutOfArrayBoundsAccess : public std::exception {};

class MyStruct
{
    uint16_t x;
    uint16_t y;

public:
    // Default constructor required for initialization in template
    MyStruct()
    {
        this->x = 0;
        this->y = 0;
    }

    MyStruct(uint16_t x, uint16_t y)
    {
        this->x = x;
        this->y = y;
    }

    // Marking operator<< as friend, so that it has access to the fields of <MyStruct>
    friend std::ostream& operator<<(std::ostream& os, const MyStruct& _this);
};

// Overloading << operator, so that I can write std::cout << myStruct;
std::ostream& operator<<(std::ostream& os, const MyStruct& _this) {
    os << "x = " << _this.x << ", y = " << _this.y;
    return os;
}


int main() {
    // Creating array of 
    //            <MyStruct>s, 
    //                |      20 of them and requiring 
    //                |       |  bounds checking.
    //                ↓       ↓    ↓
    TypedArraySafe<MyStruct, 20, true> arr1; // Create <TypedArray> of <MyStructs>. Initialized with x=0,y=0.

    // Sets third (with index 2, from 0) element with <MyStruct> where x=2,y=0.
    arr1[2] = MyStruct(5, 5);

    try
    {
        // Getting array element with index 0
        //             Here operator[] gets called
        //                  ↓
        auto zeroElem = arr1[0];

        //           Here operator<< gets called
        //             ↓
        std::cout << zeroElem << std::endl; // OUTPUTS: x = 0, y = 0
        std::cout << arr1[2] << std::endl; // OUTPUTS: x = 5, y = 5
        std::cout << arr1[21] << std::endl; // Throws exception, as expected.
    }
    catch (OutOfArrayBoundsAccess e)
    {
        std::cout << "We souldn't access element with index 21. :) " << std::endl;
    }

    std::getchar(); // Just making window, to read the output.

    return 0;

}    
```