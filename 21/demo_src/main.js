// BEGIN_REGION BaseClass
function BaseClass(firstName) {
    // This field(firstName) 
    // can be deleted, 
    // can be value changed,
    // can be enumarated.
    // 
    // That can be changed by calling
    // Object.defineProperty(this, "firstName", ...)
    this.firstName = firstName

    // This field(origin) 
    Object.defineProperty(this,"origin",{
        value:"Earth",
        writable:true, // can be value changed
        configurable:false, // can not be delted
        enumerable:true // can be enumarated
    })
}

// Adding BaseClass method sayHello
BaseClass.prototype.sayHello = function () {
    console.log("BaseClass:" + this.firstName)
}
// END_REGION BaseClass




// BEGIN_REGION DerivedClass
function DerivedClass(firstName, secondName) {
    // Calling super construtor on this (instance of DeriverClass).
    // Not strictly required thought.
    BaseClass.call(this, firstName) 

    // Adding new field
    this.secondName = secondName
}

// Here we create prototype object for DerivedClass to be set as DerivedClass.prototype
// which on it's own has prototype property pointing to BaseClass.prototype,
// so that prototype chain DerivedClass.prototype -> BaseClass.prototype -> Object.prototype 
// get's created.
DerivedClass.prototype = Object.create(BaseClass.prototype, {
    constructor: {
        value: DerivedClass // Here DevivedClass constructor get's restored
    }
})

// Or you could achieve same with helper function
// which chains derived class prototype and base class prototypes
createPrototypeChain(BaseClass,DerivedClass)

// Actual function which creates prototype chain
function createPrototypeChain(baseClass, derivedClass)
{
    derivedClass.prototype = Object.create(baseClass.prototype, {
        constructor: {
            value: derivedClass // Here DevivedClass constructor get's restored
        }
    })    
}

// !!! Remember you shell create prototype chain
// before adding new methods to class (in our case DerivedClass)

// Here we override sayHello from BaseClass
DerivedClass.prototype.sayHello = function () {
    console.log("DerivedClass: " + this.firstName + " " + this.secondName)
}

// Adding sayHelloTimes method to DeriverClass class
DerivedClass.prototype.sayHelloTimes = function (numberOfTimes) {
    console.log("\nDerivedClass saying hello " + numberOfTimes + " times")
    for (var i = 0; i < numberOfTimes; i++) {
        console.log(this.sayHello())
    }
    console.log("\n")
}

// Calling BaseClass's method sayHello from DerivedClass's method sayHelloSuper
DerivedClass.prototype.sayHelloSuper = function (){
    BaseClass.prototype.sayHello.call(this)
    console.log("\n")
}
// END_REGION DerivedClass



// BEGIN_REGION Class usage demo
var baseClassObject = new BaseClass("James")

baseClassObject.sayHello()

var derivedClassObject = new DerivedClass("James", "Bond")

derivedClassObject.sayHello() // Overriden function get's called
derivedClassObject.sayHelloTimes(3)

derivedClassObject.sayHelloSuper()

// Will output true
console.log("baseClassObject instanceof BaseClass -> " + (baseClassObject instanceof BaseClass))

// Will output true, 
// because DerivedClass derives from Base class
// and objects of type DerivedClass are isntance of
// BaseClass too.
console.log("derivedClassObject instanceof BaseClass -> " + (derivedClassObject instanceof BaseClass))

// Will output true
console.log("derivedClassObject instanceof DerivedClass -> " + (derivedClassObject instanceof DerivedClass))
// END_REGION Class usage demo