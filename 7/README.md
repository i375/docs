# Data structures VS data types

Data structures answer the question of how data is structured and saved/represented in memory. While data types answer the
question of the meaning of data in relation to other objects and processes.

For demonstration I'll define data structure `S1` which consists of two float data:


```C
struct S1
{
  float a;
  float b;
}        
```

This (`S1`) dat structure define how data is stored. 

Now I'll define three data types, each one with concrete purpose while each having same data structure:

```c
typedef struct S1 CoordinateXY;
typedef struct S1 Vector2D;
typedef struct S1 TemperatureFromTo;    
```

All three of those data types `CoordinateXY`, `Vector2D`, `TemperatureFromTo have same data structure but have special use defined by their type.

To demonstrate that I'll write three function prototypes which take and return different data types (from those three) depending
on the context.

```c
void drawPointAt(CoordianteXY pointCoordinate);
void applyPhysicalForce(RigidBody rigidBody, CoordinateXY atPoint, Vector2D forceVector);
TemperatureFromTo getLastTemperatureChange();    
```

As you can see each data type fits in the contexts it's used. And actually you could define more specialized type `ForceVector2D` based on `Vector2D` for use in `applyPhysicalForce` function, but then it's up to the experience of programmer and user for the code where to stop specializing data types.
