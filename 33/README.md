# How to chain commands in bash

Run A and B, whatever result of A is  
```
A; B 
```

Run B if A is successful  
```
A && B 
```

If A fails run B  
```
A || B 
```

Run A in background  
```
A &
```