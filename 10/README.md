# Git: Basic commands pack

Initialize git in folder:
```
git init
```

Add remote URL:
```
git remote add origin https://9221145@bitbucket.org/9221145/9221145.git //
```

Change remote URL:
```
git remote set-url origin https://9221145@bitbucket.org/9221145/9221145-framework.git
```

Clone repository or Clone repository in specific directory:
```
git clone https://9221145@bitbucket.org/9221145/9221145-framework.git
git clone https://9221145@bitbucket.org/9221145/9221145-framework.git folder-name
```

Stage all files:
```
git add --all 
```

Stage new and modified:
```
git add .     
```

Push changes from origin:
```
git pull origin master
```

Commit local changes:
```
git commit -m "Comment 123"
```

Push local changes to origin from master branch:
```
git push -u origin master
```

See git commit log:
```
git log
```

Revert to particular commit (loca changes will be lost):
```
git reset --hard 70ed60*** 
```

Track remote changes (case after removing current and adding new remote):
```
git branch --set-upstream-to=origin/[branch] master
```

Clone git repository with it's sub repositries(sub modules)<br/> Requires .gitmodules file
```
git clone --recursive https://9221145@bitbucket.org/9221145/9221145.git
```
Create branch
```
git branch new_branch
```

Swith to branch
```
git checkout branch_to_switch_to
```

Merge other branch with current branch
```
git merge other_branch_to_merge_with_current
```

Delete branch
```
git branch -d brach_to_delete
```

List branches
```
git show-brach --list
```
