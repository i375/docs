# Z Combine blurred and solid objects in Blender3D

![Picture 1: Left: Z buffers, Right: Z combined content.](Picture1.png)  
*Picture 1: Left: Z buffers, Right: Z combined content.*

I wanted to create something like shown on Picture 2 where there is a cage solid object and there is a cube it's center, which I wanted to blur later in composting stage.

![Picture 2](Picture6.png)  
*Picture 2:*

At first I created scene where there was a cage and cube in center of it like on Picture 3.
 
![](Picture3.PNG)  
*Picture 3*

And tried blurring it and Z combining, what I got was this (Picture 4):

![](Picture4.PNG)  
*Picture 4: Z buffers on left, Z combine result on right.*

It didn't work cause Z buffer for blurred cube simply wasn't there. What I needed was to imitate volume object, therefore register geometry in Z buffer, size of which would be bigger or uqual to center cube after being blurred.


So what I've done was create another cube in center bigger than the initial one, like shown in 

![](Picture5.PNG)  
*Picture 5: Adding bigger cube for generating Z buffer.*

I put cage object on RenderLayer1, center cube on RenderLayer2 and helper cube on render layer ZHelpers, as shown on Picture 1.

And while Z combining RenderLayer1 and RenderLayer2, instead of using Z from RenderLayer2 (where the center cube is) I used Z from render layer ZHelpers.

And got the result:

![](Picture6.png)

[Download sample Blender3D file here.](https://bitbucket.org/hg2vb/kb/src/master/Blender3D/ZCombineblurredandsolidobjectsinBlender3D/res/ZCombine.blend)

And that's it. :)
