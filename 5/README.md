# Windows cmd: Find file(s) with "where" command line tool

To find file(in this case notepad.exe) say on disk C:\ one shell type:

```
where /R C:\ notepad.exe        
```

To find file(s) which match some name pattern(in this case dlls which contain word system) one shell type:

```
where /R C:\ *system*.dll
where /R C:\Windows *system*.dll
where /R "c:\Program Files" system*.dll    
```

`/R` tells "where", to use recursive search in directory specified. Or if you want to narrow search results, you can redirect resulst to `findstr`:

```
where /R C:\ system*.dll | findstr "\System32"    
```

or more strict:

```
where /R C:\ system*.dll | findstr "C:\Windows\System32"        
```

This will look for results which contain directory System32 or specific directory `C:\Windows\System32`.
