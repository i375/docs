# Setup NodeJS TypeScript project and add TypeScript support for npm packages.

*Using Windows command line. Linux/macOS should not differ significantly.*

## Scenario:

- Create NodeJS project.
- Initialize TypeScript project in it.
- NPM install some package "randomstring" for generating random strings.
- Try to use "randomstring" module from TypeScript and get the errors.
- Write custom type definitions file for the module.
- Use `@types` definition file from `NPM` (if such exists), rather than using our own written types defitions file.

So let's begin:

Create NodeJS empty project

```
mkdir nodets    
```

Get in project root</p>
```
cd nodets
```

Create NodeJS project.  
Default parameters will work here. This creates `"package.json"` file.

```
npm init 
```

Initializing TypeScript project  
This creates `"tsconfig.json"`
```
tsc --init 
```

Install randomstring package.  
`--save` here will add package dependency for the project.

```
npm install randomstring --save 
```

Create `"index.ts"` file, for TypeScript programming.

```
type NUL > index.ts
```

Open file with text editor.
```
notepad index.ts
```

Copy TypeScript source code in file and save.
```Typescript
import {generate} from "randomstring"
var rndstr1 = generate()
console.log(`My random string: ${rndstr1}`)
```

Try buidling TypeScript project.
```
tsc
```

You'll get an error:
```
index.ts(1,26): error TS7016: Could not find a declaration file for module 'randomstring'. 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index.js' implicitly has an 'any' type.
    Try `npm install @types/randomstring` if it exists or add a new declaration (.d.ts) file containing `declare module 'randomstring';`
```

So as you can see compiler pretty much tells us what (`npm install @types/randomstring`) to do to provide
it with definitions file for "randomstring" module.  
But before doing so, let's analyze actually where
compiler is looking for required file(s).  
Using option `--traceResolution` will print out where TypeScript
is looking for module source code or type definitions file.

```
tsc --traceResolution
```

This outputs (reading all way throught is not neccessary):

```
======== Resolving module 'randomstring' from 'C:/Users/9221145/Documents/9221145/Experiments/nodets/index.ts'. ========
Module resolution kind is not specified, using 'NodeJs'.
Loading module 'randomstring' from 'node_modules' folder, target file type 'TypeScript'.
File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring.ts' does not exist.
File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring.tsx' does not exist.
File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring.d.ts' does not exist.
Found 'package.json' at 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/package.json'.
'package.json' does not have a 'typings' field.
'package.json' does not have a 'types' field.
File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index.ts' does not exist.
File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index.tsx' does not exist.
File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index.d.ts' does not exist.
Directory 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/@types' does not exist, skipping all lookups in it.
Directory 'C:/Users/9221145/Documents/9221145/Experiments/node_modules' does not exist, skipping all lookups in it.
Directory 'C:/Users/9221145/Documents/9221145/node_modules' does not exist, skipping all lookups in it.
Directory 'C:/Users/9221145/Documents/node_modules' does not exist, skipping all lookups in it.
File 'C:/Users/9221145/node_modules/randomstring.ts' does not exist.
File 'C:/Users/9221145/node_modules/randomstring.tsx' does not exist.
File 'C:/Users/9221145/node_modules/randomstring.d.ts' does not exist.
Directory 'C:/Users/9221145/node_modules/@types' does not exist, skipping all lookups in it.
Directory 'C:/Users/node_modules' does not exist, skipping all lookups in it.
Directory 'C:/node_modules' does not exist, skipping all lookups in it.
Loading module 'randomstring' from 'node_modules' folder, target file type 'JavaScript'.
File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring.js' does not exist.
File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring.jsx' does not exist.
Found 'package.json' at 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/package.json'.
'package.json' has 'main' field './index' that references 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index'.
File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index' does not exist.
Loading module as file / folder, candidate module location 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index', target file type 'JavaScript'.
File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index.js' exist - use it as a name resolution result.
Resolving real path for 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index.js', result 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index.js'.
======== Module name 'randomstring' was successfully resolved to 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index.js'. ========
index.ts(1,26): error TS7016: Could not find a declaration file for module 'randomstring'. 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index.js' implicitly has an 'any' type.
    Try `npm install @types/randomstring` if it exists or add a new declaration (.d.ts) file containing `declare module 'randomstring';`
```

There are many places compiler is looking but lets filter out most important, folders and files where compiler is looking for type defitions files (`.d.ts`)

```
File 'C:/Users/9221145/node_modules/randomstring.d.ts' does not exist.

File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring.d.ts'

File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index.d.ts' does not exist.

Directory 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/@types' does not exist, skipping all lookups in it.
```

Let's create `@types` folder

```
cd node_modules
mkdir @types
cd ..
```

and rerun
```
tsc --traceResolution
```

Here are filtered out interesting locations

```
File 'C:/Users/9221145/node_modules/randomstring.d.ts' does not exist.

File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring.d.ts' does not exist.

File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index.d.ts' does not exist.

File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/@types/randomstring.d.ts' does not exist.
```

These places pretty much make sense right. With one exception that if module itselft doesn't provide type definition file by default, let's not mess up with it's root folder, in this case:
```
File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring/index.d.ts' does not exist.
```

In this case I'm not going to share defitions file among many proejcts so I'll keep things under project root, so leave out this options too:
```
File 'C:/Users/9221145/node_modules/randomstring.d.ts' does not exist.
```

And whats left are:
```
File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/randomstring.d.ts' does not exist.

File 'C:/Users/9221145/Documents/9221145/Experiments/nodets/node_modules/@types/randomstring.d.ts' does not exist.
```

These are safe for messing with. Just to keep thing a bit organized I'll create type defitions file `randomstring.d.ts` for `randomstring` package under `@types` folder.

```
cd node_modules
cd @types
type NUL > randomstring.d.ts
notepad randomstring.d.ts
```

Copy and save code, this is super basic (just to keep things simple) minimum defitions for `randomstring` module, it just describes module and function `generate` in it, which returns `string`:
```
export declare function generate():string
```

Not let's get to prject root and rebuild project.
```
cd ..\..
tsc
dir
```

As you can see `index.js` file was created successfully:
```
11.09.2017  23:18               199 index.js
11.09.2017  21:50               100 index.ts
11.09.2017  23:00    &lt;DIR>          node_modules
11.09.2017  22:44               558 package-lock.json
11.09.2017  22:44               256 package.json
11.09.2017  22:21             4 668 tsconfig.json
```

So now run NodeJS app:
```
node index.js
```

It shell output (your random string will be different, obviously)
```
My random string: V9IREGKn81zzdONzM7wwVfPBOlOuAsTz
```

Now let's erase our custom `randomstring.d.ts` file and `npm install` it.
```
cd node_modules
cd @types
del randomstring.d.ts
cd ..\..
npm install @types/randomstring
```

Then build and run project:
```
tsc
node index.js
```

Ouput sould be something like this (your random string will be different, obviously):
```
My random string: Z5ZoIV7dx3BihmtiHoHhnAsETXaggwff
```

That's it. :)

Here is project archive for [custom version](demo_src/nodets_custom_types.zip)  and [npm `@type` version](demo_src/nodets.zip)