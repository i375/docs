# IGameMechanic - Game mechanic generic _model.

**Game is an interactive process, with a goal or goals which can be achieved or lost. Even endless game has a goal of at least maxing out result.** 

And, interactive experiences like particle simulators which you can play with your pointer (or finger) or ineractive movies
(without choyces, leeding to win or loss) don't quite count as games.  
Well one might say that interactive experiences can be games of achieving some kind of mental state and failling
to do so (like feeling good or shitty) could be called loosing and doing so, winning.  
But then mainstream drugs and drinks alike are games too and whole idea of game breaks down and everything
becomes game and the word game itself looses it's meaning and becomes eqaul to infinity and everything and infinity
is not a number and so on, in short shit starts to flow.

Thought game itself is an experience, every experience is not a game. 

So again back to the game.  
Game needs some kind of dynamic mechanic, it's hard to imagine one frame picture as a game. Ok, the board can
be one frame but still the mental process would be dynamic for whatever game would be played on one frame board like
chess or checker or something like it.  
After there is some basic or super complex dynamic process, it needs to have some interaction points. Say if
it's a water flowing down in free fall, this is a dynamic process and you can interact with it with you hands or
other solids, liquids of super high energies. 

Then it needs to be winnable and therefore lossable.  
This means having some sort of constraint. Like water being finite, and wind is flowing, and you need to direct
as much water as possible in to a jar.  
You learn how to feel water and wind and after trial and error you develop an intuition (or calucalated moves
which could be described formaly not requiring intuision, depending on game style), and as the intuition gets better
and better you'd redirect more and more water in to a jar, untill the maximum amount of water is used.
