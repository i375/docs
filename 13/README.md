# Unity3D graphics programming links

[⇱ Shader introduction](https://unity3d.com/learn/tutorials/topics/graphics/gentle-introduction-shaders)

[⇱ HLSL Intrinsic Functions](https://msdn.microsoft.com/en-us/library/windows/desktop/ff471376(v=vs.85).aspx)

[⇱ HLSL Semantics](https://msdn.microsoft.com/en-us/library/windows/desktop/bb509647(v=vs.85).aspx)