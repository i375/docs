# Callbacks, lambdas, and deterministic destruction and resource managment in PHP

```php
class MyClass
{
    private $customCallback;

    function __construct($customCallback = null)
    {
        $this->customCallback = $customCallback;

        printf("- created ".get_class($this)."<br/>");
    }

    function __destruct()
    {
        if($this->customCallback)
        {
            $this->customCallback->call($this);
        }

        printf("- destructing ".get_class($this)." <br/>");
    }
}


function testDeterministicDestruction()
{
    $someResource = "some resource";

    $myClass = new MyClass(function() use ($someResource){
        printf("- performing custom clean for ".$someResource."<br/>");
    });

    printf("- some computation in scope/block inside ".__FUNCTION__."<br/>");
}

printf("- before calling testDeterministicDestruction<br/>");

testDeterministicDestruction();

printf("- after calling testDeterministicDestruction<br/>");
```

**As expected,Outputs:** 

```
- before calling testDeterministicDestruction 
- created MyClass 
- some computation in scope/block inside testDeterministicDestruction 
- performing custom clean for some resource 
- destructing MyClass 
- after calling testDeterministicDestruction
```