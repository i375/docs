# Defining structure and behaviour of classes and objects statically with Generics in TypeScript and passing values as Generic's parameters. (C# sample included)

*Article is not necessarily for learning ideas (which you can of cource), but more for demonstrating how to implement familiar ideas in TypeScript too.*

Before I delve into details, I shell say that TypeScript is my favorite extension to JavaScript, cause it brings static types
system/safety, ability to write self-documenting code, and code manageability in contrast to the utter nonsense that
you have to deal with if you write raw JavaScript.

TypeScript compiler used: version 2.14.

What you see below is attempt to construct classes and objects so that their features and partially behaviour is defined
at compile time, as much as possible. But ass you'll see you can't avoid some runtime code writing in TypeScript,
just because of JavaScript idiosyncrasies.


```TypeScript
class ValueContainer<ValueType>;
{
    private value: ValueType;

    protected setValue(value: ValueType) {
        this.value = value
    }

    getValue(): ValueType {
        return this.value
    }
}

class NumberValue extends ValueContainer<number>{ }

class StringValue extends ValueContainer<string>{ }

class _2000 extends NumberValue {
    constructor() {
        super()
        this.setValue(2000)
    }
}

class _2017 extends NumberValue {
    constructor() {
        super()
        this.setValue(2017)
    }
}

class _PassCode1 extends StringValue {
    constructor() {
        super()
        this.setValue("super hard passcode 1")
    }
}

class _PassCode2 extends StringValue {
    constructor() {
        super()
        this.setValue("super hard passcode 2")
    }
}

enum DoorState {
    Open,
    Locked,
    Closed,
    Unlocked
}

class Door<PassCode extends StringValue, YearOfProduction extends NumberValue>
{
    state: DoorState
    passCode: PassCode
    yearOrProduction: YearOfProduction

    constructor()
    {
        this.passCode = new PassCode() <b>// Compiler error: 'PassCode' only refers to a type, but is being used as a value here. (Something you'd expect to be able to do)</b>

        this.yearOrProduction = new YearOfProduction() <b>// Compiler error: 'YearOfProduction' only refers to a type, but is being used as a value here. (Something you'd expect to be able to do)</b>
    }

    tryUnlocking(passCode:string):DoorState
    {
        if(passCode == this.passCode.getValue())
        {
            this.state == DoorState.Unlocked
        }

        return this.state
    }
}

// Here we try to define <SuperOldDoor> with it's features statically
// <b>But this won't be enough, we'll have to write some runtime code too, as you'll see below.</b>
class SupperOldDoor extends Door<_PassCode2, _2000>{}

// Here we try to define <NewDoor> with it's features statically
// <b>But this won't be enough, we'll have to write some runtime code too, as you'll see below.</b>
class NewDoor extends Door<_PassCode1, _2017>{}

function main()
{
    var oldDoor = new SupperOldDoor()
    var newDoor = new NewDoor()

    if(oldDoor.tryUnlocking("pass 1") == DoorState.Unlocked)
    {
        console.log(oldDoor.yearOrProduction)
    }
}
```

The reason to this limitation is that Genenics' parameters `SomeGeenerc<GenericParameterT>` carry no type information to when compiled to JavaScript.


But there is [a limited work around](https://github.com/Microsoft/TypeScript/issues/2037#issuecomment-74402010) this issue (of not being able to use Generics parameters for instantiating objects), which I use below. The difference is, now we pass generic parameter `Type` objects as values through super constructor, at runtime.

Using  
```Typescript
interface NoParamConstructor<T> {
    new (): T;
}
```
capability.

Differences are highlighted. As you can see there's more runtime code that one would like to have, but still better than
nothing. Besides that Generics we have in our disposal not full featured [compile time template system](https://en.wikipedia.org/wiki/Template_metaprogramming).

```TypeScript
class ValueContainer<ValueType>
{
    private value: ValueType;

    protected setValue(value: ValueType) {
        this.value = value
    }

    getValue(): ValueType {
        return this.value
    }
}

class NumberValue extends ValueContainer<number>{ }

class StringValue extends ValueContainer<string>{ }

class _2000 extends NumberValue {
    constructor() {
        super()
        this.setValue(2000)
    }
}

class _2017 extends NumberValue {
    constructor() {
        super()
        this.setValue(2017)
    }
}

class _PassCode1 extends StringValue {
    constructor() {
        super()
        this.setValue("super hard passcode 1")
    }
}

class _PassCode2 extends StringValue {
    constructor() {
        super()
        this.setValue("super hard passcode 2")
    }
}

enum DoorState {
    Open,
    Locked,
    Closed,
    Unlocked
}
<span class="highlight bold">
interface NoParamConstructor<T> {
    new (): T;
}
</span>
class Door<PassCode extends StringValue, YearOfProduction extends NumberValue>
{
    state: DoorState
    passCode: PassCode
    yearOrProduction: YearOfProduction

    constructor(<span class="highlight bold">PassCode_constructor:NoParamConstructor<PassCode>, YearOfProduction_constructor:NoParamConstructor<YearOfProduction></span>)
    {
        this.passCode = <span class="highlight bold">new PassCode_constructor()</span> // runtime support

        this.yearOrProduction = <span class="highlight bold">new YearOfProduction_constructor()</span> // runtime support 
    }

    tryUnlocking(passCode:string):DoorState
    {
        if(passCode == this.passCode.getValue())
        {
            this.state = DoorState.Unlocked
        }

        return this.state
    }
}

// Here we define <SupperOldDoor> with it's features, statically 
// at compile time + runtime construtor "sugar"
class SupperOldDoor extends Door<_PassCode2, _2000>{
<span class="highlight bold">    constructor()
    {
        super(_PassCode2,_2000)
    }</span>
}

// Here we define <NewDoor> with it's features, statically 
// at compile time + runtime construtor "sugar"
class NewDoor extends Door<_PassCode1, _2017>{
<span class="highlight bold">    constructor()
    {
        super(_PassCode1,_2017)
    }</span>
}

function main()
{
    var oldDoor = new SupperOldDoor()
    var newDoor = new NewDoor()

    if(oldDoor.tryUnlocking("super hard passcode 2") == DoorState.Unlocked)
    {
        console.log(oldDoor.yearOrProduction.getValue())
    }
}

main()        
```

As a bonus C# implementation:

```C#
namespace FeaturesAndBehaviours
{
    class ValueContainer<ValueType>
    {
        protected ValueType value;

        public ValueType Value
        {
            get
            {
                return this.value;
            }
        }
    }

    class NumberValue : ValueContainer<int> { }

    class StringValue : ValueContainer<string> { }

    class _2000 : NumberValue
    {
        public _2000()
        {
            this.value = 2000;
        }
    }

    class _2017 : NumberValue
    {
        public _2017()
        {

            this.value = 2017;
        }
    }

    class _PassCode1 : StringValue
    {
        public _PassCode1()
        {
            this.value  = "super hard passcode 1";
        }
    }

    class _PassCode2 : StringValue
    {
        public _PassCode2()
        {
            this.value = "super hard passcode 2";
        }
    }

    enum DoorState
    {
        Open,
        Locked,
        Closed,
        Unlocked
    }

    class Door<PassCode, YearOfProduction> 
    where PassCode : StringValue, new() /* Here we say that parameter is something constructible */
    where YearOfProduction : NumberValue, new() /* Here we say that parameter is something constructible */
    {
        public DoorState state;
        public PassCode passCode = new PassCode();
        public YearOfProduction yearOrProduction = new YearOfProduction();
        
        public DoorState tryUnlocking(string passCode)
        {
            if (passCode == this.passCode.Value)
            {
                this.state = DoorState.Unlocked;
            }

            return this.state;
        }
    }

    // Here we try to define <SuperOldDoor> with it's features statically
    class SupperOldDoor : Door<_PassCode2, _2000> { }

    // Here we try to define <NewDoor> with it's features statically
    class NewDoor : Door<_PassCode1, _2017> { }

    class Main
    {
        public static void main()
        {
            var oldDoor = new SupperOldDoor();
            var newDoor = new NewDoor();

            if (oldDoor.tryUnlocking("super hard passcode 2") == DoorState.Unlocked)
            {
                System.Console.WriteLine(oldDoor.yearOrProduction.Value);
            }
        }
    }
}    
```


External resource:

- [Generics (TypeScript handbook)](https://www.typescriptlang.org/docs/handbook/generics.html)
- [Constraints on Generics Type Parameters (C# Programming Guide)](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/generics/constraints-on-type-parameters)
  
Happy semi-static programming!