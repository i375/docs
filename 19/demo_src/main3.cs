namespace FeaturesAndBehaviours
{
    class ValueContainer<ValueType>
    {
        protected ValueType value;

        public ValueType Value
        {
            get
            {
                return this.value;
            }
        }

    }

    class NumberValue : ValueContainer<int> { }

    class StringValue : ValueContainer<string> { }

    class _2000 : NumberValue
    {
        public _2000()
        {
            this.value = 2000;
        }
    }

    class _2017 : NumberValue
    {
        public _2017()
        {

            this.value = 2017;
        }
    }

    class _PassCode1 : StringValue
    {
        public _PassCode1()
        {
            this.value = "super hard passcode 1";
        }
    }

    class _PassCode2 : StringValue
    {
        public _PassCode2()
        {
            this.value = "super hard passcode 2";
        }
    }

    enum DoorState
    {
        Open,
        Locked,
        Closed,
        Unlocked
    }

    class Door<PassCode, YearOfProduction> where PassCode : StringValue, new() where YearOfProduction : NumberValue, new()
    {
        public DoorState state;
        public PassCode passCode = new PassCode();
        public YearOfProduction yearOrProduction = new YearOfProduction();
        
        public DoorState tryUnlocking(string passCode)
        {
            if (passCode == this.passCode.Value)
            {
                this.state = DoorState.Unlocked;
            }

            return this.state;
        }
    }

    // Here we try to define <SuperOldDoor> with it's features statically
    class SupperOldDoor : Door<_PassCode2, _2000> { }

    // Here we try to define <NewDoor> with it's features statically
    class NewDoor : Door<_PassCode1, _2017> { }

    class Main
    {
        public static void main()
        {
            var oldDoor = new SupperOldDoor();
            var newDoor = new NewDoor();

            if (oldDoor.tryUnlocking("super hard passcode 2") == DoorState.Unlocked)
            {
                System.Console.WriteLine(oldDoor.yearOrProduction.Value);
            }
        }
    }
}
