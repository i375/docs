class ValueContainer<ValueType>
{
    private value: ValueType;

    protected setValue(value: ValueType) {
        this.value = value
    }

    getValue(): ValueType {
        return this.value
    }
}

class NumberValue extends ValueContainer<number>{ }

class StringValue extends ValueContainer<string>{ }

class _2000 extends NumberValue {
    constructor() {
        super()
        this.setValue(2000)
    }
}

class _2017 extends NumberValue {
    constructor() {
        super()
        this.setValue(2017)
    }
}

class _PassCode1 extends StringValue {
    constructor() {
        super()
        this.setValue("super hard passcode 1")
    }
}

class _PassCode2 extends StringValue {
    constructor() {
        super()
        this.setValue("super hard passcode 2")
    }
}

enum DoorState {
    Open,
    Locked,
    Closed,
    Unlocked
}

interface NoParamConstructor<T> {
    new (): T;
}

class Door<PassCode extends StringValue, YearOfProduction extends NumberValue>
{
    state: DoorState
    passCode: PassCode
    yearOrProduction: YearOfProduction

    constructor(PassCode_constructor:NoParamConstructor<PassCode>, YearOfProduction_constructor:NoParamConstructor<YearOfProduction>)
    {
        this.passCode = new PassCode_constructor() // Compiler error: 'PassCode' only refers to a type, but is being used as a value here. (Something you'd expect to be able to do)

        this.yearOrProduction = new YearOfProduction_constructor() // Compiler error: 'YearOfProduction' only refers to a type, but is being used as a value here. (Something you'd expect to be able to do)
    }

    tryUnlocking(passCode:string):DoorState
    {
        if(passCode == this.passCode.getValue())
        {
            this.state = DoorState.Unlocked
        }

        return this.state
    }
}

// Here we try to define <SuperOldDoor> with it's features statically
class SupperOldDoor extends Door<_PassCode2, _2000>{
    constructor()
    {
        super(_PassCode2,_2000)
    }
}

// Here we try to define <NewDoor> with it's features statically
class NewDoor extends Door<_PassCode1, _2017>{
    constructor()
    {
        super(_PassCode1,_2017)
    }
}

function main()
{
    var oldDoor = new SupperOldDoor()
    var newDoor = new NewDoor()

    if(oldDoor.tryUnlocking("super hard passcode 2") == DoorState.Unlocked)
    {
        console.log(oldDoor.yearOrProduction.getValue())
    }
}

main()