var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ValueContainer = (function () {
    function ValueContainer() {
    }
    ValueContainer.prototype.setValue = function (value) {
        this.value = value;
    };
    ValueContainer.prototype.getValue = function () {
        return this.value;
    };
    return ValueContainer;
}());
var NumberValue = (function (_super) {
    __extends(NumberValue, _super);
    function NumberValue() {
        return _super.apply(this, arguments) || this;
    }
    return NumberValue;
}(ValueContainer));
var StringValue = (function (_super) {
    __extends(StringValue, _super);
    function StringValue() {
        return _super.apply(this, arguments) || this;
    }
    return StringValue;
}(ValueContainer));
var _2000 = (function (_super) {
    __extends(_2000, _super);
    function _2000() {
        var _this = _super.call(this) || this;
        _this.setValue(2000);
        return _this;
    }
    return _2000;
}(NumberValue));
var _2017 = (function (_super) {
    __extends(_2017, _super);
    function _2017() {
        var _this = _super.call(this) || this;
        _this.setValue(2017);
        return _this;
    }
    return _2017;
}(NumberValue));
var _PassCode1 = (function (_super) {
    __extends(_PassCode1, _super);
    function _PassCode1() {
        var _this = _super.call(this) || this;
        _this.setValue("super hard passcode 1");
        return _this;
    }
    return _PassCode1;
}(StringValue));
var _PassCode2 = (function (_super) {
    __extends(_PassCode2, _super);
    function _PassCode2() {
        var _this = _super.call(this) || this;
        _this.setValue("super hard passcode 2");
        return _this;
    }
    return _PassCode2;
}(StringValue));
var DoorState;
(function (DoorState) {
    DoorState[DoorState["Open"] = 0] = "Open";
    DoorState[DoorState["Locked"] = 1] = "Locked";
    DoorState[DoorState["Closed"] = 2] = "Closed";
    DoorState[DoorState["Unlocked"] = 3] = "Unlocked";
})(DoorState || (DoorState = {}));
var Door = (function () {
    function Door() {
        this.passCode = new PassCode(); // Compiler error: 'PassCode' only refers to a type, but is being used as a value here. (Something you'd expect to be able to do)
        this.yearOrProduction = new YearOfProduction(); // Compiler error: 'YearOfProduction' only refers to a type, but is being used as a value here. (Something you'd expect to be able to do)
    }
    Door.prototype.tryUnlocking = function (passCode) {
        if (passCode == this.passCode.getValue()) {
            this.state == DoorState.Unlocked;
        }
        return this.state;
    };
    return Door;
}());
// Here we try to define <SuperOldDoor> with it's features statically
var SupperOldDoor = (function (_super) {
    __extends(SupperOldDoor, _super);
    function SupperOldDoor() {
        return _super.apply(this, arguments) || this;
    }
    return SupperOldDoor;
}(Door));
// Here we try to define <NewDoor> with it's features statically
var NewDoor = (function (_super) {
    __extends(NewDoor, _super);
    function NewDoor() {
        return _super.apply(this, arguments) || this;
    }
    return NewDoor;
}(Door));
function main() {
    var oldDoor = new SupperOldDoor();
    var newDoor = new NewDoor();
    if (oldDoor.tryUnlocking("pass 1") == DoorState.Unlocked) {
        console.log(oldDoor.yearOrProduction);
    }
}
