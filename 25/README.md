# How history, books and logs matters

We are not born with knowledge of mathematics and logic. And we don't wait for evolution to make us species with built in mathematics and logics skill. Instead what we do is write books and put _the evolution_ in them, so that new and current generations can read and acquire new and more advanced skill sets.

Books are like knowledge logs. And above said is how logs and books matter.

Same goes for history. We are not born with knowledge and instincts to know that some things are destructive than constructive, like nuclear wars, different types economical and political structures. And if we tried to wait for evolution to make the past knowledge and experience built in our brains, we might as well be dead before that happened, cause there's limited number of times you can have say nuclear war for it to be hardwired in brains of new generations through evolutionary process.

We write history logs in history books so that we don't have to make experiment each time test weather one way or other is dangerous, destructive or constructive, effective or ineffective. And this is how history research, books and their refinement to make them more accurate matters.
