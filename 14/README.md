# Measurement unit metric prefixes

```
yotta	Y	10^24	1000000000000000000000000		septillion
zetta	Z	10^21	1000000000000000000000			sextillion
exa	E	10^18	1000000000000000000			quintillion
peta	P	10^15	1000000000000000			quadrillion
tera	T	10^12	1000000000000				trillion
giga	G	10^9	1000000000				billion
mega	M	10^6	1000000					million
kilo	k	10^3	1000					thousand
hecto	h	10^2	100					hundred
deca	da	10^1	10					ten
		10^0	1					one
deci	d	10^−1	0.1					tenth
centi	c	10^−2	0.01				        hundredth
milli	m	10^−3	0.001					thousandth
micro	μ	10^−6	0.000001				millionth
nano	n	10^−9	0.000000001				billionth
pico	p	10^−12	0.000000000001				trillionth
femto	f	10^−15	0.000000000000001			quadrillionth
atto	a	10^−18	0.000000000000000001			quintillionth
zepto	z	10^−21	0.000000000000000000001			sextillionth
yocto	y	10^−24	0.000000000000000000000001		septillionth
```