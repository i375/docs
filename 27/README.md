# Copying text to clipboard in command line

_recepies for copying text to clipboard on different platforms_

## On windows

Copy contents of text file to clipboard
```
type some_file.txt | clip
```

Copy text to clipboard
```
echo some text | clip
```

Pasting
```
SHIFT+INSERT or CTRL+V
```

If having problems copying UTF-8 text run
```
chcp 65001
```

## On macOs

Copy contents of text file to clipboard
```
cat some_file.txt | pbcopy
```

Copy text to clipboard
```
echo some text | pbcopy
```

Pasting
```
pbpaste
```

## Remaks

Also you can use any file as a temprorary data storage, just like clipboard, and
it will be as convenient as methods described above, if you don't need to take
copied text out of terminal to some other GUI app.
