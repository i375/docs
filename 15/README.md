# Program/Computation is about data transformation, programming languages are about expressing transformation, performance, and management. *Programming is data oriented process*

*When talking about programming languages I mean that they are [turing complete.](https://en.wikipedia.org/wiki/Turing_completeness)*

Title might sound bit weird in sense that it states something obvious or not so obvious if you are surounded by programmers
or non progammers who frequently ask "Can that be done in this or that programming language?" question, while all
there is to be done is computing data `B` from data `A`, which doesn't depend on language features.

Anyways this little is for re-stating the fact (for my self in first place) that program (that you can actually run on computer)
is about computing data `B` from data `A`, where `A` can be `user input`, `some other data`,
`sensor input`, `...` and `B` can be `audio data`,`text data`, `graphics(like next game frame)`,`...` .


You might encounter and be part of various debates, conversations, processes about this or that programming language, weather
this or that programming language can solve this or that problem.


There might form an illusion that differences between programming languages are about features which decide weather
program can be programmed (in principle) for solving the problem at hand, while completely missing the fact that programming languages
and their diferences are about satisfying set of constraints, some of which are: development process management stability,
cost, and maintainability; minimum required knowledge for being able to do actual work; ability to describe program
in most efficient way to satify performance or memory constraints;... .

This article about to remind that no matter which language used, programs/computation/programming is about data transformation.

Note for beginner: If you are at the beggining of programming and are about to choose which programming language to start with, choose any, libraries of which have support for basic user input (keyboard, mouse, touch) and output (sound, graphics, text). Everything else (further choices) will become self evident when you'll start pushing the limits of performance, memory, manageability, and cost of development.
