# Replace string-substring in JavaScript

Replace all occasions:
```JavaScript
var someString = "Boom box has many box in it"

var replacedString = someString.replace(new RegExp("box","g"),"cucumber")
```

Replace first occasion:
```JavaScript
var someString = "Boom box has many box in it"

var replacedString = someString.replace(new RegExp("box"),"cucumber")
```

Helper functions
```
function replaceString(what, withWhat, from)
{
	return from.replace(new RegExp(what,),withWhat)
}

function replaceStringAll(what, withWhat, from)
{
	return from.replace(new RegExp(what,"g"),withWhat)
}
```
