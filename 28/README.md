# CST - C Structures as Text standard (alpha version)

_Representing C structs as Text_

## Suported types with default values 

```
C type                    Default value (first symbols on line)      Comment
---------------------------------------------------------------------------------------------------------------------------------------

int                       0                                          For integers 0 is default value
float                     0 or 0.0                                   For floats/doubles 0.0 is default value
empty char*/string        0 (for empty, followed by any)             Empty string, everything after 0 is ignored
non empty char*/string    @ followed with string                     Non empty string everyithing after @ is used, even if more than one blank space
void*                     .                                          Represents end of dynamic array, NULL, if it's a first symbol on a line. 
struct                    .                                          Compound type of all types above.
```

## Rules

- Blank spaces at the beggining of line are igonred.
- Non emptry strings start with ```@``` symbol, followed with actual string
- Empty strings are start with ```0``` symbol, followed with anything which is ignored
- Data ordering in text matters, like ```text_line[0]``` corresponds to ```field[0]```, ```text_line[1]``` to ```field[1]```, etc. ... 
- Each field have it's own line.
- Fields of type string (char\*, std::string, string) are represented on one line and
for multiline strings ```\n``` new line character is used.
  - Non empty strings start with  ``` ```  (blank space) as a first symbol on a line.
- If fieldis a compount type (struct) it will be written on multiple lines, where each
ofthe field a line is give to.
- Dynamic and fixed sized arrays are supported.
  - If array is dynamic last item is denoted by ```.``` value on a new line (example bellow).
  - If array is fixed size one header field is added which contains items count (example below).

## Examples

### int
C code:
```c
int a = 0;
```
Text:
```
0
```

### float
C code:
```c
float a = 0;
```
Text:
```
0.0
```

### Empty char\*/string:
C code:
```c
char* a = "";
```
Text:
```
0
```

### Non-empty multi line char\*/string:
C code:
```c
char* a = "hello there\n next line";
```
Text (see on second example blank space before string, this can be used formating):
```
@hello there\n next line
```
or (indeted, for optional readablity purposes)
```
 @hello there\n next line
```

### Non empty array int\*
C code:
```c
int* a = {1,2};
```
Text:
```c
1
2
.
```

### Empty array int\*
C code:
```c
int* a = {};
```
Text:
```c
.
```

## Full demo

Struct A in C:

```c
struct A
{
  int        a;
  float      b;
  char*      c;
  struct B*  d;
  int        e[2];
  float*     f;
  int8       g;
};

struct B
{
  int     a;
  char*   b;
  int    *c;
}
```

Struct A data as text:

```c
Data (text content)                       Type      Name        Comment
-------------------------------------------------------------------------------------------------------------------------------------------
0                                         int       A.a            Default integral value         
0.0                                       float     A.b            Default float value             
0                                         char*     A.c            Empty string
  123                                     int       A.d[0].a   
  @some string abc \n line 2 txt          char*     A.d[0].b       Multi line string example
    .                                               A.d[0].c[END]  This is an empty array, it contains 0 elements and has only END symbol.
  234                                     int       A.d[1].a
  @some string 123                        char*     A.d[1].b
    1                                     int       A.d[1].c[0]
    2                                     int       A.d[1].c[1]
    .                                               A.d[1].c[END]  End of array
  .                                                 A.d[END]       End of array of B structures
  345                                     int       A.e[0]         As A.e array with know size, it woun't have . in the end
  0.345                                   int       A.e[1] 
  1.0                                     float     A.f[0]
  2.0                                     float     A.f[1]
  .                                                 A.f[END]       Empty value, marks end of array A.f
-128                                      int8      A.g            Negative integral value
```
